<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repository extends Model
{
    //

    protected $fillable=['repos_name','repos_url','user_id'];
}
