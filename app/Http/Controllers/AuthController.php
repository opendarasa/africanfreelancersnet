<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Repositories\Repository as Repos;
use App\Repository;
use Auth;
use App\User;
class AuthController extends Controller
{
    //
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    protected $model;
    public function __construct(User $user){
        $this->middleware('auth');
        $this->model=new Repos($user);
    }

    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('github')->user();
        
        $this->getUserRepos($user);
        return redirect()->back();
        // $user->token;
    }

    public function changePic(Request $request)
    {
        $file=$request->file('image');

        $filename=$this->model->saveUserImage($file);

        $user=User::find(Auth::user()->id);
        $user->image=$filename;
        $user->save();

        return redirect()->back();
    }



    private function getUserRepos($user)
    {

     $name=$user->getNickname();
     $token=$user->token;
     // We generate the url for curl
     $curl_url = 'https://api.github.com/users/' .$name. '/repos';

     // We generate the header part for the token
     $curl_token_auth = 'Authorization: token ' . $token;

     // We make the actuall curl initialization
     $ch = curl_init($curl_url);

     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

     // We set the right headers: any user agent type, and then the custom token header part that we generated
     curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: Awesome-Octocat-App', $curl_token_auth));

     // We execute the curl
     $output = curl_exec($ch);

    // And we make sure we close the curl       
    curl_close($ch);

     // Then we decode the output and we could do whatever we want with it
     $output = json_decode($output);
     foreach ($output as  $repo) {
      Repository::create([
                'repos_name'=>$repo->name,
                'repos_url'=>$repo->html_url,
                'user_id'=>Auth::user()->id,
      ]);
    }
  }
}
