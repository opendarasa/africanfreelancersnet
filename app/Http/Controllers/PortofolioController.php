<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portofolio;
use App\Repositories\Repository;
use Auth;
class PortofolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    public function __construct(Portofolio $work)
    {
        //$this->middleware('auth');

        $this->model=new Repository($work);
    }
    public function index()
    {
        $works=$this->model->allById();
        $asc_works=$this->model->all();


        return view('portofolio')->with(compact('works','asc_works'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('create_new_work');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=$this->model->validateWork($request);
        if($validation['status']==true)
        {
            $data=$validation['data'];
            $work=$this->model->create($data);
            return redirect()->route('portofolio.edit',['slug'=>$work->slug]);
        }
        else{
            return redirect()->back()->withErrors($validation['data'])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        

        return redirect()->route('users.show',['username'=>Auth::user()->username]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $work=$this->model->show($slug);

        return view('edit_portofolio')->with(compact('work'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        //
        $check=Portofolio::where('slug',$slug)->count();
        if ($check>0) {
            $validation=$this->model->validateUpdatedWork($request,$slug);

        if($validation['status']==true)
        {
            $data=$validation['data'];
            $this->model->update($data,$slug);
            return redirect()->route('portofolio.edit',['slug'=>$data['slug']]);
        }
        else{
            return redirect()->back()->withErrors($validation['data'])->withInput();
          }
        }else{
            return redirect('home');
        }

         

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->model->delete($id);

        return redirec()->back();
    }
}
