<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\Blog;
use Validator;
use App\Portofolio;
use App\User;
use DB;
use Session;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    public function __construct(Blog $blog)
    {
        $this->model=new Repository($blog);
        $this->middleware(['auth','password-changed'],['except' => ['index','show','searchByKeyword','voteArticle']]);
    }

    public function index()
    {   
        $blogs=$this->model->allById();
        $users=DB::table('users')->join('blogs','blogs.user_id','=','users.id')->where('blogs.status',1)->select('users.*')->groupBy('users.id')->orderBy('blogs.votes','desc')->limit(3)->get();
        $recommendations = Blog::where('status',1)->orderBy('votes','desc')->limit(6)->get();
        return view('blog')->with(compact('blogs','users','recommendations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create_blog_post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=$this->model->validateBlog($request);

        if ($validation['status']==true) {

        $data=$validation['data'];

        $blog=$this->model->create($data);

        Session::flash('status',trans('app.blog_created_successfuly'));
        return redirect()->route('blog.edit',[$blog->slug]);
          
      }else{
        return redirect()->back()->withErrors($validation['data'])->withInput();
      }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        
        $currentBlog=$this->model->show($slug);
        if($currentBlog){
            $currentBlog->views +=1;
            $currentBlog->save();
        }
        $user=User::find($currentBlog->user_id);
        $latest_works=Portofolio::where('status',1)->orderBy('id','desc')->limit(5)->get();
        $blogs=Blog::where('status',1)->where('id','!=',$currentBlog->id)->where('cat_id',$currentBlog->cat_id)->orderBy('votes','desc')->limit(5)->get();

        return view('view_blog')->with(compact('currentBlog','latest_works','blogs','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $blog=$this->model->show($slug);

        return view('edit_blog')->with(compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $check=Blog::where('slug',$slug)->count();
        if($check>0)
        {
             $validation=$this->model->validateUpdatedBlog($request,$slug);
        if($validation['status']==true)
        {
            $data=$validation['data'];
            $this->model->update($data,$slug);
            Session::flash('status',trans('app.blog_edited_successfuly'));
            return redirect()->route('blog.edit',[$data['slug']]);
        }else{
            return redirect()->back()->withErrors($validation['data'])->withInput();
        }

        }else{
            return redirect('home');
        }
        
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model->delete($id);
    }

    public function searchByKeyword(Request $request)
    {
        if(!$request->has('keyword') || $request->keyword == ''){
            return abort(404);
        }
        $keyword = $request->keyword;
        $blogs = Blog::where(function ($query) use ($keyword) {
                    $query->whereRaw('LOWER(title) LIKE ?', ['%' . strtolower($keyword) . '%'])
          ->orWhereRaw('LOWER(subtitle) LIKE ?', ['%' . strtolower($keyword) . '%'])
          ->orWhereRaw('LOWER(tags) LIKE ?', ['%' . strtolower($keyword) . '%']);
            })->orderBy('votes','desc')->get();

        $recommendations = Blog::where('status',1)->orderBy('views','desc')->limit(6)->get();

        return view('search-result')->with(compact('blogs','recommendations'));
    }

    public function voteArticle(Request $request){
        if(!$request->id || !$request->vote){
            return response()->json(['status'=>'error','message'=>trans('app.invalid_request'),'data'=>[]]);
        }
        $blog=Blog::find($request->id);
        if(!$blog){
             return response()->json(['status'=>'error','message'=>trans('app.invalid_request'),'data'=>[]]);
        }

        $blog->votes = (int)$blog->votes + (int)$request->vote;
        $blog->save();
        return response()->json(['status'=>'success','message'=>trans('app.success'),'data'=>$blog->votes]);
    }

    
}
