<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\User;
use Auth;
use App\Repository as Repos;
use App\Blog;
use App\Portofolio;
use Mail;
use App\Mail\Welcome;
use Session;
use DB;
use Validator;
use Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    public function __construct(User $user)
    {
      //$this->middleware('auth');
      $this->model=new Repository($user);
      $this->middleware(['auth','password-changed'],['except'=>['show','changePassword']]);
      

    }
    public function index()
    {
        //
        $users=$this->model->allUsersById();
        $asc_users=$this->model->allUsers();
        

        return view('users')->with(compact('users','asc_users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        if (Auth::check() && Auth::user()->admin!=1) {
            return redirect()->back();
        }
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validation=$this->model->validateUser($request);
        if($validation['status']==true)
        {
            $user=$this->model->create($validation['data']);
            $users=$this->model->allUsersById();
            $email=$user->email;
            $pass=$request->input('password');
            Mail::to($email)->bcc('support@opendarasa.com')->queue(new Welcome($email,$pass));
            return redirect()->route('users.index');
        }
        else{
            return redirect()->back()->withErrors($validation['data'])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        //


        $user=$this->model->showUser($username);

        $repos=Repos::where('user_id',$user->id)->get();

        $blogs=Blog::where('user_id',$user->id)->where('status',1)->orderBy('updated_at','desc')->get();
        $works=Portofolio::where('user_id',$user->id)->where('status',1)->orderBy('created_at','desc')->get();
        $unpublishedBlogs=Blog::where('user_id',$user->id)->where('status',0)->orderBy('updated_at','desc')->get();
        $unpublishedWorks=Portofolio::where('user_id',$user->id)->where('status',0)->orderBy('created_at','desc')->get();
        
        return view('view_user')->with(compact('user','repos','blogs','works','unpublishedWorks','unpublishedBlogs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($username)
    {
        //

        $user=$this->model->showUser($username);

        return view('view_user')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $username)
    {
        //
        if(isset($request->admin)){
            $request->admin = 1;
        }else{
            $request->admin = 0;
        }
        $user=User::where('username',$username)->first();
        $validation=$this->model->ValidateUpdateUser($request,$user);
        if($validation['status']==true)
        {
            $this->model->updateUser($validation['data'],$username);
            $newUser = User::where('username',$username)->first();
            $this->savePasswordReset($newUser);
           return $this->show($username);
        }
        else{
             return redirect()->back()->withErrors($validation['data'])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->model->delete($id);
        $defaultAdmin = User::where('username','digits21')->first();
        Blog::where('user_id',$id)->update(['user_id'=>$defaultAdmin->id]);
        return redirect()->back();
    }

    public function addSkills(Request $request)
    {
        $user=User::find($request->input('user_id'));
        $user->skills.=','.$request->input('skills');
        $user->save();

        return redirect()->back();
    }


     // update password

    public function changePassword(Request $request){
        if ($request->all()) {
            $validator = Validator::make($request->all(),
                [
                    'currentPassword' => 'required|string',
                    'password' => 'required|string|min:6|confirmed',
                ]
            );
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $user=User::find(Auth::user()->id);
            if ($user) {
                # code...
                if (Hash::check($request->currentPassword,$user->password)) {
                     $data=[
                        'password'=>Hash::make($request->password),
                     ];


                     $user->update($data);


                     $this->savePasswordReset($user);
                     return redirect()->back()->with('status',trans('app.password_changed'));

                }else{
                   return redirect()->back()->with('errorMsg',trans('app.wrong_password'));
                }
            }else{
                 return redirect()->back()->with('errorMsg',trans('app.user_not_found'));
            }
        }else{
          return redirect()->back()->with('errorMsg',trans('app.something_wrong_happened'));
        }

    }


     public function savePasswordReset($user)
    {

            $token=Str::random(60);

            $password_reset=DB::table('password_resets')->where('email',$user->email)->orderBy('created_at','desc')->limit(1)->get();
            
            if ($password_reset->count() > 0) {
                
                DB::table('password_resets')->where('email',$user->email)->update([
                    'token'=>$token,
                    'created_at'=>Carbon::now(),
                        ]);
            }else{
                    DB::table('password_resets')->insert([
                    'email'=>$user->email,
                    'token'=>$token,
                    'created_at'=>Carbon::now(),
                 ]);
            }               

    }

    public function removeSkill($skill)
    {
        $skills = explode(',' ,Auth::user()->skills);
        if(count($skills) > 0 ){
            $keyToRemove = array_search($skill, $skills);
            unset($skills[$keyToRemove]);
            $newSkills = implode(",", $skills);
            $user = Auth::user();
            $user->skills = $newSkills;
            $user->save();
            return redirect()->back();
        }
    }

}
