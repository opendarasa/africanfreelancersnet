<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Portofolio;
use App\User;
use App\Mail\ThankYouForYourMessage;
use Mail;
use App\Repositories\Repository;
use App;
use Session;
use DB;
use Validator;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs=Blog::where('status',1)->orderBy('votes','desc')->limit(6)->get();
        $works=Portofolio::where('status',1)->orderBy('id','desc')->limit(6)->get();
        $users=DB::table('users')->join('blogs','blogs.user_id','=','users.id')->where('blogs.status',1)->select('users.*')->groupBy('users.id')->orderBy('blogs.views','desc')->limit(3)->get();
        $recommendations = Blog::where('status',1)->orderBy('votes','desc')->limit(6)->get();

        return view('home')->with(compact('blogs','works','users','recommendations'));
    }

    public function sendMessage(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'email'=> 'required|email',
            'phone'=> 'required|regex:/^\+(?:[0-9] ?){6,14}[0-9]$/',
            'message'=>'required|string',
        ]);
         $url = url('home').'#contact';
        if ($validator->fails()) {
           
            return redirect($url)->withErrors($validator)->withInput();
        }
        $name=$request->input('name');
        $email=$request->input('email');
        $phone=$request->input('phone');
        $message=$request->input('message');

        Mail::to('ouedmenga@gmail.com')->send(new ThankYouForYourMessage($name,$email,$phone,$message));
        
        return redirect($url)->with('status',trans('app.we_have_received_your_message'));

    }

    public function publishBlog($slug)
    {
        $this->model=new Repository(new Blog());
        $blog=$this->model->show($slug);
        $blog->status=1;
        $blog->save();
        return redirect()->route('blog.show',[$blog->slug]);
    }
    public function publishWork($slug)
    {
        $this->model=new Repository(new Portofolio());
        $work=$this->model->show($slug);
        $work->status=1;
        $work->save();
        return redirect()->route('portofolio.show',[$work->slug]);
    }

    public function unpublishBlog($slug)
    {
        $this->model=new Repository(new Blog());
        $blog=$this->model->show($slug);
        $blog->status=0;
        $blog->save();
        return redirect()->back();
    }
    public function unpublishWork($slug)
    {
        $this->model=new Repository(new Portofolio());
        $work=$this->model->show($slug);
        $work->status=0;
        $work->save();
        return redirect()->route('portofolio.show',[$work->slug]);
    }
    public function changeLanguage($lang)
    {
        

       Session::put('lang',$lang);
       App::setLocale($lang);
       //echo App::getLocale();
       
       return redirect()->back();

    }
}
