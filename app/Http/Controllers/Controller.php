<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App;
use Session;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct()
    {
    	$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    	switch ($lang) {
    		case 'en':
    			App::setLocale('fr');
    			break;
    		
    		default:
    		     if (Session::has('lang')) {
    		     	$lang=Session::get('lang');
    		     	
    		     }else{
    		     	App::setLocale('fr');
    		     }
    			
    			break;
    	}
    }
}
