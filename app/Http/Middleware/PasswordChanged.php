<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use DB;
class PasswordChanged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (isset(Auth::user()->email)) {
             $password_reset_record = DB::table('password_resets')->where('email', Auth::user()->email)->get();
            if ($password_reset_record->count() == 0) {
                return redirect('update-password')->with('errorMsg',trans('app.you_need_to_update_your_password'));
            }
        }
       
        return $next($request);
    }
}
