<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ThankYouForYourMessage extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *$name,$email,$phone,$message
     * @return void
     */
    public $name;
    public $email;
    public $phone;
    public $contactMessage;
    public function __construct($name,$email,$phone,$contactMessage)
    {
        //
        $this->name=$name;
        $this->email=$email;
        $this->phone=$phone;
        $this->contactMessage=$contactMessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('app.new_message'))->view('emails.thankyou');
    }
}
