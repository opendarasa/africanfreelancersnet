@extends('layouts.newcase-template')


@section('content')
<section class="bg-light" id="portfolio">
  <div class="container">
          <div class="row" style="margin-bottom:5%;">
            <div class="col-sm-12">
               @include('shared.search-form')
            </div>
          </div>
            @if($blogs->count() == 0)
             <div class="row" style="text-align:center;background-color:#807c7c;">
                <div class="col-sm-12">
                  <h5>{{trans('app.no_search_result')}}</h5>
                </div>    
            </div>
            <hr>
            <h6>{{trans('app.our_recommendation')}}</h6>
              @foreach($recommendations as $recommendation)
                <div class="row" style="height:60%;background-color:#fff; margin-top:1%;width:100%;">
                  <div class="col-sm-10">
                    <a href="{{url('blog/'.$recommendation->slug)}}">
              
                      <h6>{{(strlen($recommendation->title)>20)?mb_substr(strip_tags($recommendation->title),0,70).'..':$recommendation->title}}</h6>
                      <p class="text-muted">{{(strlen($recommendation->subtitle))?mb_substr(strip_tags($recommendation->subtitle),0,100).'..':$recommendation->subtitle}}</p>
                      <p class="text-muted">{{(strlen($recommendation->content))?mb_substr(strip_tags($recommendation->content),0,200).'..':$recommendation->content}}</p>
                    </a>
                  </div>
                  <div class="col-sm-2">
                    <img class="img-fluid" src="{{asset('img/blog/'.$recommendation->image)}}" alt="">
                  </div>
                </div>
              @endforeach

            @else
              @foreach($blogs as $blog)
                <div class="row" style="height:60%; background-color:#fff;margin-top:1%;width:100%;">
                  <div class="col-sm-10">
                    <a href="{{url('blog/'.$blog->slug)}}">
              
                      <h6 style="text-align:center;">{{(strlen($blog->title)>20)?mb_substr(strip_tags($blog->title),0,70).'..':$blog->title}}</h6>
                      <p class="text-muted">{{(strlen($blog->subtitle))?mb_substr(strip_tags($blog->subtitle),0,100).'..':$blog->subtitle}}</p>
                      <p class="text-muted">{!!(strlen($blog->content))?mb_substr(strip_tags($blog->content),0,200).'..':$blog->content!!}</p>
                    </a>
                  </div>
                  <div class="col-sm-2">
                    <img class="img-fluid" src="{{asset('img/blog/'.$blog->image)}}" alt="">
                  </div>
                </div>
              @endforeach
        @endif   
        
  </div>
      
</section>

  @endsection