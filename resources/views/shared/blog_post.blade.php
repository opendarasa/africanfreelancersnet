
<div class="row">
  <div class="col-md-2" style="position:auto">
    @foreach(trans('app.post_categories') as $key => $value)
      <a href="{{url('category/'.str_slug($value,'-'))}}" class="badge badge-dark badge-pill">
        {{$value}}
      </a>
        <hr>
    @endforeach
  </div>
  <div class="col-md-10">
    @foreach($blogs as $blog)
          <div class="col-md-{{(count($blogs)==1)?12:8}} col-sm-{{(count($blogs)==1)?12:6}} portfolio-item" id="desc">
            <a class="portfolio-link" href="{{route('blog.show',$blog->slug)}}">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-eye fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/blog/'.$blog->image)}}" alt="">
            </a>
            <div class="portfolio-caption">
              <a href="{{route('blog.show',$blog->slug)}}">
                <h4>{{(strlen($blog->title)>20)?mb_substr(strip_tags($blog->title),0,20).'..':$blog->title}}</h4>
                <p class="text-muted">{{(strlen($blog->subtitle))?mb_substr(strip_tags($blog->subtitle),0,60).'..':$blog->subtitle}}</p>
              </a>
              <p>
                <?php $tags=explode(",",$blog->tags); ?>
            @foreach($tags as $tag)
            {{'#'.$tag}}

            @endforeach
              </p>
              <p>
               @if(Auth::check() && Auth::user()->id==$blog->user_id)
                 <a href="{{route('blog.edit',$blog->slug)}}">
                  <i class="fa fa-pencil"></i>
                </a>
                @endif
                <i class="fa fa-clock-o">
                  
               </i>
              {{(new Carbon\Carbon($blog->created_at))->diffForHumans()}}</p>
            </div>

          </div>
@endforeach
  </div>

@if(isset($asc_blogs))
@foreach($asc_blogs as $blog)
          <div style="display:none;" class="col-md-{{(count($asc_blogs)==1)?12:4}} col-sm-{{(count($asc_blogs)==1)?12:6}} portfolio-item" id="asc">
            <a class="portfolio-link" href="{{route('blog.show',$blog->slug)}}">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-eye fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/blog/'.$blog->image)}}" alt="">
            </a>
            <div class="portfolio-caption">
              <a href="{{route('blog.show',$blog->slug)}}">
                <h4>{{mb_substr(strip_tags($blog->title),0,20)}}...</h4>
                <p class="text-muted">{{mb_substr(strip_tags($blog->subtitle),0,60)}}...</p>
              </a>
              <p>
                <?php $tags=explode(",",$blog->tags); ?>
            @foreach($tags as $tag)
            {{'#'.$tag}}

            @endforeach
              </p>
              <p>
                @if(Auth::check() && Auth::user()->id==$blog->user_id)
                 <a href="{{route('blog.edit',$blog->slug)}}">
                  <i class="fa fa-pencil"></i>
                </a>
                @endif
                <i class="fa fa-clock-o" ></i>
                
                  
                </i>
                {{(new Carbon\Carbon($blog->created_at))->diffForHumans()}}</p>
            </div>

          </div>
@endforeach
@endif
@if(Auth::check()&& isset($user) && Auth::user()->id==$user->id && isset($unpublishedBlogs))

@foreach($unpublishedBlogs as $blog)
          <div  class="col-md-{{(count($unpublishedBlogs)==1)?12:4}} col-sm-{{(count($unpublishedBlogs)==1)?12:6}} portfolio-item" id="asc">
            <a class="portfolio-link" href="{{route('blog.show',$blog->slug)}}">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-eye fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/blog/'.$blog->image)}}" alt="">
            </a>
            <div class="portfolio-caption">
              @if(count($blogs)<=2)
              <h4>{{mb_substr(strip_tags($blog->title),0,10)}}...</h4>
              @else
              <h4>{{mb_substr(strip_tags($blog->title),0,20)}}...</h4>
              @endif
              <p class="text-muted">{{mb_substr(strip_tags($blog->subtitle),0,60)}}...</p>
              <p>

                <?php $tags=explode(",",$blog->tags); ?>
            @foreach($tags as $tag)
            {{'#'.$tag}}

            @endforeach
              </p>

              <p>
                <a href="{{route('blog.edit',$blog->slug)}}">
                  <i class="fa fa-pencil"></i>
                </a>
                <i class="fa fa-lock" style="color:red;"></i>
                <i class="fa fa-clock-o">
                  
                </i>
                {{(new Carbon\Carbon($blog->created_at))->diffForHumans()}}</p>
            </div>

          </div>
@endforeach
@endif
@if(count($blogs)>6)
<div style="text-align:center;">
    <a href="{{url('portofolio')}}" class="btn btn-primary">{{trans('app.see_more')}}</a>
</div>
@endif
</div>