 {{csrf_field()}}
          <div class="form-group">
            {{Form::text('title',null,['class'=>'form-control','placeholder'=>trans('app.title').'*'])}}
            <!--<input placeholder="title" type="text" name="title" class="form-control" value="">-->
            
          </div>
           <div class="form-group">
            {{Form::textarea('subtitle',null,['class'=>'form-control','placeholder'=>trans('app.subtitle').'*','maxlength'=>500])}}
            <!--<input placeholder="subtitle" type="text" name="subtitle" class="form-control" value="">-->
          </div>
          <div class="form-group">
            @php
              $catId = (isset($blog))?$blog->cat_id:old('cat_id');
            @endphp
            {{Form::label('type',trans('app.select_type').'*')}}
            {{Form::select('cat_id',trans('app.post_categories'),$catId,['class'=>'form-control','id'=>'type'])}}
            
          </div>
          <div class="form-group">
            <!--<label for="editor">Blog content</label>-->
            {{Form::label('editor',trans('app.content').'*')}}
           <!-- <textarea name="content"  class="form-control" id="editor">
             
            </textarea>-->
            {{Form::textarea('content',null,['class'=>'form-control','id'=>'editor'])}}
          </div>
          <div class="form-group">
            
            {{Form::label('myfile',trans('app.blog_image').'*')}}
             {{Form::file('image',['id'=>'myfile'])}}

            <!--<input id="input-id" data-show-upload="false" type="file"  class="file" name="image">-->
            <div id="kartik-file-errors"></div>
          </div>
          <div class="form-group">
             
             {{Form::text('tags',null,['class'=>'form-control','placeholder'=>trans('app.tags_input_placeholder')])}}
            <!--<input placeholder="tags, separate by [,]" type="text" name="tags" class="form-control" value="">-->
          </div>

    
          