@if(isset($asc_users))
  
@include('shared.sort')
@endif
<div id="list">
@foreach($users as $user)
<div class="container" id="desc">
    <div class="card flex-row flex-wrap">
        <div class="card-header border-0">
            <img class="profileimage" src="{{asset('img/users/'.$user->image)}}" alt="">
        </div>
        <div class="card-block px-2">
            
                <h4 class="card-title">
                <span>
                    {{$user->name}} 
                     
                </span>
               
               </h4>
                @if(Auth::check() && Auth::user()->id != $user->id && Auth::user()->admin == 1)   
                    <form method="POST" action="{{ route('users.destroy', [$user->id]) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button title="Delete" class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Êtes-vous sûre de vouloir suprimer ce compte?')"><i class="fa fa-fw fa-trash"></i></button>
                    </form>   
                @endif
            
            <span class="text-muted">{{trans('app.worked_at')}}: {{$user->company}}</span>
            <p class="card-text">{{mb_substr(strip_tags($user->description),0,100)}}...</p>
            <p class="card-text">
                {{(isset($user->experience))?$user->experience:''}} {{trans('app.years_experience')}}
            </p>
            <p>
                
                <?php $skills=explode(",",$user->skills);
                 
                 ?>
                @if(count($skills)>1)
                <h6>{{trans('app.skills')}}</h6>
                @foreach($skills as $skill)
                <a href="#" class="badge badge-dark badge-pill">{{$skill}}
                </a>
                @endforeach

                @else
                <h6>{{trans('app.no_skills-earned_yet')}}</h6>
                @endif

            </p>
            <p>
                <i class="fa fa-map-marker">
                    {{$user->location}}
                </i>
            </p>
           
             
             <div style="display:inline-block;">
                 <a  href="{{route('users.show',$user->username)}}" class="btn btn-info">{{trans('app.view_user')}}</a> 
             </div>
            
            <br><br>
          </div>

        <div class="w-100"></div>
        <div class="card-footer w-100 text-muted">
            {{trans('app.joined').'  '.(new Carbon\Carbon($user->created_at))->diffForHumans()}}
        </div>
    </div>
    <br>
</div>
@endforeach
</div>

<script type="text/javascript">
    function Mysort() {
    // Declare variables
    var input, filter, listDiv, span, h4, i;
    input = document.getElementById('mysearch');
    filter = input.value.toUpperCase();
    listDiv = document.getElementById("list");
    h4 = listDiv.getElementsByTagName('h4');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < h4.length; i++) {
        span = h4[i].getElementsByTagName("span")[0];
        var parentDiv=h4[i].parentNode.parentNode.parentNode;
        if (span.innerHTML.toUpperCase().indexOf(filter) > -1) {
            parentDiv.style.display = "";
        } else {
            parentDiv.style.display = "none";
        }
    }
}
</script>
