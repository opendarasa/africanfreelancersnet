<form class="form-inline" action="{{url('search')}}">       
  <div class="form-group col-md-12">
    <div class="col-md-1">
              
    </div>
    <div class="col-md-9">
        <input type="" name="keyword" class="form-control" placeholder="{{trans('app.search_book')}}" style="width:100%;">
    </div>
    <div class="col-md-1">
        <button class="btn btn-default form-control pull-right" type="submit" style="background-color:#4B65C8;color: #FFF;">
          {{trans('app.search')}}
        </button>
    </div>
            
  </div>    
</form>