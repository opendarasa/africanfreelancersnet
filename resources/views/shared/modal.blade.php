@foreach($works as $work)
    <div class="portfolio-modal modal fade" id="portfolioModal{{$work->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">{{$work->title}}</h2>
                  <p class="item-intro text-muted">{{$work->subtitle}} </p>
                  <img style="width:100%; height:700px;" class="img-responsive img-fluid d-block" src="{{asset('img/portfolio/'.$work->image)}}" alt="">
                  <p>{!!$work->content!!}</p>
                  <ul class="list-inline">
                    <li>{{trans('app.date')}}: {{$work->created_at}}
                    </li>
                    <li>{{trans('app.client')}}: {{$work->client}}
                    </li>
                    <li>{{trans('app.Category')}}: {{config('category.category_'.$work->cat_id)}}
                    </li>

                  </ul>
                  <p>
                <?php $tags=explode(",",$work->tags); ?>
                     @foreach($tags as $tag)
                      {{'#'.$tag}}
                     @endforeach
                  </p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    {{trans('app.close')}}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   @endforeach
   @if(isset($asc_works))
   @foreach($asc_works as $work)
    <div class="portfolio-modal modal fade" id="portfolioModal{{$work->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">{{$work->title}}</h2>
                  <p class="item-intro text-muted">{{$work->subtitle}} </p>
                  <img style="width:100%; height:700px;" class="img-responsive img-fluid d-block" src="{{asset('img/portfolio/'.$work->image)}}" alt="">
                  <p>{!!$work->content!!}</p>
                  <ul class="list-inline">
                    <li>{{trans('app.date')}}: {{$work->created_at}}
                    </li>
                    <li>{{trans('app.client')}}: {{$work->client}}
                    </li>
                    <li>{{trans('app.Category')}}: {{config('category.category_'.$work->cat_id)}}
                    </li>

                  </ul>
                  <p>
                <?php $tags=explode(",",$work->tags); ?>
                     @foreach($tags as $tag)
                      {{'#'.$tag}}
                     @endforeach
                  </p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    {{trans('app.close')}}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   @endforeach
   @endif
   @if(isset($unpublishedWorks))
@foreach($unpublishedWorks as $work)
<div class="portfolio-modal modal fade" id="portfolioModal{{$work->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">{{$work->title}}</h2>
                  <p class="item-intro text-muted">{{$work->subtitle}} </p>
                  <img style="width:100%; height:700px;" class="img-responsive img-fluid d-block" src="{{asset('img/portfolio/'.$work->image)}}" alt="">
                  <p>{!!$work->content!!}</p>
                  <ul class="list-inline">
                    <li>{{trans('app.date')}}: {{$work->created_at}}
                    </li>
                    <li>{{trans('app.client')}}: {{$work->client}}
                    </li>
                    <li>{{trans('app.Category')}}: {{config('category.category_'.$work->cat_id)}}
                    </li>

                  </ul>
                  <p>
                <?php $tags=explode(",",$work->tags); ?>
                     @foreach($tags as $tag)
                      {{'#'.$tag}}
                     @endforeach
                  </p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    {{trans('app.close')}}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endforeach
   @endif