<div class="row">
  <!-- best authors -->
  <div class="col-md-12" style="text-align:center">
    <h5>{{trans('app.our_best_authors_of_the_month')}}</h5>
    @foreach($users as $user)
      <div class=" col-sm-3 card" style="text-align:center">
        <div class="fakeimg">
          <a href="{{url('users/'.$user->username)}}">
            <img src="{{asset('img/users/'.$user->image)}}">
            <h5>{{$user->username}}</h5>
          </a>
          <p>
              {{trans('app.joined')}}
            <i class="fa fa-clock-o">                   
            </i>
                {{(new Carbon\Carbon($user->created_at))->diffForHumans()}}</p>
        </div>    
      </div>
    @endforeach
  </div> 
</div>