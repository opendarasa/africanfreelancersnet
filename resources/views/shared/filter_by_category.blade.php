<script>
    document.getElementById('filter').addEventListener('click', function (event) {
        if (event.target.tagName === 'A') {
            event.preventDefault(); // Prevent the default link behavior
            if (event.target.classList.contains('active')) {
               event.target.classList.remove('active');
               location.reload()
            }else{
              var clickedValue = event.target.innerText.trim();
              var categoryItems = document.querySelectorAll('.category');
            

            // Iterate over each list item in .category
              categoryItems.forEach(function (item) {
                  var categoryValue = item.innerText.trim();

                  // Check if the clicked value is different from the category value
                  if (clickedValue !== categoryValue) {
                      // Hide the list item
                      console.log(item.parentNode.parentNode.parentNode)
                      item.parentNode.parentNode.parentNode.style.display = 'none';
                  } else {
                      // If the clicked value matches the category value, show the list item
                      item.parentNode.parentNode.parentNode.style.display = 'block';
                  }
              });

               // Remove the 'active' class from all links and add it to the clicked link
              var links = document.querySelectorAll('#filter a');
              links.forEach(function (link) {
                  link.classList.remove('active');
              });
              event.target.classList.add('active');
              }
              
        }
    });
</script>