

<div class="row-with-filter">
  <div class="row fixed-filter-row" style="text-align:center;" >
    <div class="col-md-1">
      
    </div>
    <div class="col-md-10" id="filter">
      @foreach(trans('app.post_categories') as $key => $value)
        <a href="" class="badge badge-dark badge-pill" style="display: inline-block;">
          {{$value}}
        </a>
      @endforeach
    </div>
  
  </div>
</div>
  

<div class="row content-margin">

  <div class="col-md-8 col-xs-offset-2">
    @foreach($blogs as $blog)
          <div class="col-md-{{(count($blogs)==1)?12:12}} col-sm-{{(count($blogs)==1)?12:12}} portfolio-item" id="desc">
            <a class="portfolio-link" href="{{route('blog.show',$blog->slug)}}">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-eye fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/blog/'.$blog->image)}}" alt="">
            </a>
            <div class="portfolio-caption">
              <a href="{{route('blog.show',$blog->slug)}}">
                <h4>{{(strlen($blog->title)>20)?mb_substr(strip_tags($blog->title),0,20).'..':$blog->title}}</h4>
                <p class="text-muted">{{(strlen($blog->subtitle))?mb_substr(strip_tags($blog->subtitle),0,60).'..':$blog->subtitle}}</p>
              </a>
              <p>
                <?php $tags=explode(",",$blog->tags); ?>
            @foreach($tags as $tag)
            {{'#'.$tag}}

            @endforeach
              </p>
              <p>
               @if(Auth::check() && Auth::user()->id==$blog->user_id)
                 <a href="{{route('blog.edit',$blog->slug)}}">
                  <i class="fa fa-pencil"></i>
                </a>
                @endif
               
               <p>
                  <small class="category"> {{trans('app.post_categories')[$blog->cat_id]}}</small>
               </p>
               <p>
                 <i>
                   {{trans('app.revelancy')}} : {{$blog->votes}}
                 </i>
               </p>
                <i class="fa fa-clock-o">
                  
               </i>
              {{(new Carbon\Carbon($blog->created_at))->diffForHumans()}}</p>
            </div>

          </div>
    @endforeach
    
    
  </div>
  <!-- our recommendations -->
  <div class="col-md-4" style="text-align:center">
    <h5>{{trans('app.our_recommendation')}}</h5>
    @foreach($recommendations as $post)
      <div class="card" style="text-align:center">
        <div class="fakeimg">
          <a href="{{url('blog/'.$post->slug)}}">
          
             <h6>{{(strlen($post->title)>20)?mb_substr(strip_tags($post->title),0,25).'..':$post->title}}</h6>
              <p class="text-muted">{{(strlen($post->subtitle))?mb_substr(strip_tags($post->subtitle),0,60).'..':$post->subtitle}}</p>
          </a>
          <p>
              {{trans('app.posted')}}
            <i class="fa fa-clock-o">                   
            </i>

                {{(new Carbon\Carbon($post->created_at))->diffForHumans()}}</p>
        </div>    
      </div><br>
    @endforeach
  </div>
</div>

@include('shared.filter_by_category')
