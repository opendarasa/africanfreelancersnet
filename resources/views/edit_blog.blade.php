@extends('layouts.newcase-template')


@section('content')

      <div class="container">
        
          <div class="col-sm-12 text-center">
            @if($blog->status==0)
            <div><small>({{trans('app.only_you_can_see_this')}})</small></div>
            @endif
            <h2 style="display:inline-block;" class="section-heading text-uppercase">
            {{trans('app.edit_blog')}}

            </h2>

            @if($blog->status==0)

            <a style="display: inline-block;margin-left:2%;margin-bottom:3%;" href="{{action('HomeController@publishBlog',$blog->slug)}}" class="btn btn-danger btn-sm"
              onclick="return confirm('{{trans('app.are_you_sure_you_want_publish')}}')"
              >
              {{trans('app.publish')}}
            </a>
            @else
             <a style="display: inline-block;margin-left:2%;margin-bottom:3%;" href="{{action('HomeController@unpublishBlog',$blog->slug)}}" class="btn btn-warning"
              onclick="return confirm('{{trans('app.are_you_sure_you_want_unpublish')}}')"
              >
              {{trans('app.unpublish')}}
            </a>
            @endif
            <a style="display: inline-block;margin-left:2%;margin-bottom:3%;" href="{{route('blog.show',$blog->slug)}}" class="btn btn-info btn-sm" target="_blank">
              {{trans('app.preview')}}
            </a>
            
          </div>
        
        
      <div class="col-sm-12">
        @if(session()->has('status'))
        <div class="alert alert-success">
          {{session('status')}}
        </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger">
          {{$errors->first()}}
        </div>
        @endif
         {{Form::model($blog, [
         'method' => 'PATCH',
         'route' => ['blog.update', $blog->slug],
         'files'=>'true',
         ]) }}
          @include('shared.shared_form')

          <div class="form-group">
            {{Form::submit(trans('app.save'),['class'=>'btn btn-info pull-right','style'=>'background-color:#4B65C8;'])}}

            <!--<button type="submit" class="btn btn-info pull-right" name="save">
              Save
            </button>-->
            
          </div>
        {{Form::close()}}
      </div>
        
        
      </div>
    
  
@endsection