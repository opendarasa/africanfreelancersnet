@extends('layouts.newcase-template')


@section('content')
<div id="blog">
  <section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row" style="margin-bottom:4%;">
          <div class="col-sm-12">
            @include('shared.search-form')
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.latest_bog_post')}}</h2>
          </div>
        </div>
        
          @include('shared.home_blogs')
           
      </div>
      
</section>
</div>


  @endsection