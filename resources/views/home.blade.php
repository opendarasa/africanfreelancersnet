@extends('layouts.index')

@section('title')
 {{config('app.name')}}
@endsection
@section('portfolio')
@include('shared.grid')
@endsection
@section('team')
@include('shared.users')
@endsection
@section('blog')
@include('shared.home_blogs')
 <div class="col-md-8 col-xs-offset-2">
    <div style="text-align:center;">
          <a href="{{route('blog.index')}}" class="btn btn-sm" style="background-color:#4B65C8; color:white;">
          {{trans('app.see_more')}}
          </a>
    </div>
</div>
@endsection
@section('modals')
@include('shared.modal')
@endsection

@section('pricing')
@include('shared.pricing')
@endsection

