@extends('layouts.newcase-template')


@section('content')

      <div class="container">
        
          <div class="col-sm-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.new_work')}}</h2>
            
          </div>
        
        
      <div class="col-sm-12">
        @if($errors->any())
        <div class="alert alert-danger">
          {{$errors->first()}}
        </div>
        @endif
           {{Form::open( array(
             'route' => 'portofolio.store', 
             'class' => 'form', 
             'files' => true))}}
           @include('shared.shared_form')

           <div class="form-group">
               {{Form::text('client',null,['class'=>'form-control','placeholder'=>trans('app.client_name')])}}
             <!--<input type="text" name="client" class="form-control" placeholder="Client name">-->
           </div>
         
          <div class="form-group">
            {{Form::submit(trans('app.save'),['class'=>'btn btn-info pull-right'])}}
            
          </div>
           
         </form>
      </div>
        
        
      </div>
    
  
@endsection