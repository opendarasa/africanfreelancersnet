

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @if(isset($currentBlog))
    <meta name="og:description" content="{{$currentBlog->subtitle}}">
    @elseif(isset($work))
    <meta name="og:description" content="{{$work->subtitle}}">
    @elseif(isset($user))
    <meta name="description" content="{{trans('app.skills').': '.$user->skills.' ,'.trans('app.location').':/ '.$user->location}}">
    @else
    <meta name="description" content="{{trans('app.description')}}">
    @endif
    
    @if(isset($currentBlog))
    <meta name="og:image" content="{{asset('img/blog/'.$currentBlog->image)}}">
    @elseif(isset($work))
    <meta name="og:image" content="{{asset('img/portfolio/'.$work->image)}}">
    @elseif(isset($user))
    <meta name="og:image" content="{{asset('img/users/'.$user->image)}}">
    @else
    <meta name="og:image" content="{{asset('img/banner_50.png')}}">
    @endif
    
    <meta name="author" content="">
    <meta name="keywords" content="{{trans('app.keywords')}}">
    @if(isset($currentBlog))
    <title>{{$currentBlog->title}}</title>
    @elseif(isset($work))
    <title>{{$work->title}}</title>
    @elseif(isset($user))
    <title>{{$user->username}}</title>
    @else
    <title>@yield('title')</title>
    @endif
    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('css/carousel.css')}}">
    <!-- Custom fonts for this template -->
    <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
  
    <!-- Custom styles for this template -->
    <link href="{{asset('css/agency.min.css')}}" rel="stylesheet">
    <!-- dependency: React.js -->
    <script src="{{asset('js/react-with-addons.js')}}"></script>
    <script src="{{asset('js/react-dom.js')}}"></script>
    <script src="{{asset('js/browser.min.js')}}"></script>

    <!-- Literally Canvas -->
    
  <style type="text/css">
    .carouselImage{
      width: 90px; 
      height: 90px; 
    margin: 0 auto;
    border-radius: 50%;
    border: 1px solid #5db4c0;
    margin-top:2%;
    margin-bottom:0%;
    }
    .mystudies {
      margin-bottom:5%;
      
    }
    .myskills{
      margin-bottom:5%;
      margin-top: 5%;
    }
    .profileimage{
    width:150px;
    height: 150px;
}
.container{
    margin-top:0%;
}

.row-with-filter{
    position: relative;
    z-index: 100000; /* Set a higher z-index to ensure it appears above other elements */
  }
  .fixed-filter-row {
    position: sticky;
    top: 15%;
    width: 100%;
    background-color: #fff; /* Add your desired background color */
    padding: 10px; /* Optional: Add some padding for styling */
    z-index: 1000;
  }

  /* Add a margin to the content below the fixed row to prevent it from being hidden */
  .content-margin {
    margin-top: 40px;
    
  }

  </style>
  