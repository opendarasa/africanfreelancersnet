 <!-- Navigation -->
    <nav style="background: white;" class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{url('home')}}">
          <img src="{{asset('img/500design.png')}}" width="30%">
          <span style="font-family:Tahoma; color:#AED9D6;"></span>
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a style="color:black;" class="nav-link js-scroll-trigger" href="{{url('home')}}"></a>
            </li>
            <li class="nav-item dropdown" style="display:none;">
 <a style="color:black;" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
  {{ trans('app.my_services') }} 
  <span class="caret">
  </span>
  </a>

<div style="position:auto;"  class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
  <a style="color:black;" class="dropdown-item" href="{{url('new-web-case')}}">{{trans('app.web_dev')}}
  </a>
  <a style="color:black;" class="dropdown-item" href="{{url('new-ui-case')}}">{{trans('app.mobile_dev')}}
  </a>
  <a style="color:black;" class="dropdown-item" href="{{url('new-seo-case')}}">{{trans('app.seo')}}
  </a>
  
  <a style="color:black;" class="dropdown-item" href="{{url('services')}}">{{trans('app.see_all')}}
  </a>
  
  
  </div>
  </li>
            <li class="nav-item" style="display: none;">
              <a style="color:black;" class="nav-link js-scroll-trigger" href="{{url('portofolio')}}">{{trans('app.my_work')}}</a>
            </li>
           
            <li class="nav-item">
              <a style="color:black;" class="nav-link js-scroll-trigger" href="{{route('blog.index')}}">{{trans('app.read')}}</a>
            </li>
             <li class="nav-item">
              <a style="color:black;" class="nav-link js-scroll-trigger" href="{{route('blog.create')}}">{{trans('app.write')}}</a>
            </li>
            <!--<li class="nav-item">
              <a style="color:black;" class="nav-link js-scroll-trigger" href="{{url('services')}}">{{trans('app.pricing')}}</a>
            </li>-->
            @if(Auth::check())
            
             <li class="nav-item dropdown">
             <a style="color:black;" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              <i class="fa fa-user"></i>
              <span class="caret">
              </span>
              </a>

            <div style="position:auto;"  class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a style="color:black;" class="dropdown-item" style="font-size:10pt;">
                Salut {{Auth::user()->name}}
              </a>
              <a style="color:black;" class="dropdown-item" href="{{route('users.show',Auth::user()->username)}}">{{trans('app.profile')}}
              </a>
              @if(Auth::user()->admin==1)
              <a style="color:black;" class="dropdown-item" href="{{route('users.create')}}">{{trans('app.new_user')}}
              </a>
              <a style="color:black;" class="dropdown-item" href="{{route('users.index')}}">{{trans('app.view_users')}}
              </a>
              @endif
              <a style="color:black;" class="dropdown-item" href="{{url('update-password')}}">{{trans('app.change_password')}}</a>
              <a style="color:black;" class="dropdown-item" href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                    {{ __('app.Logout') }}
                                                </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
              </form>
            </div>
      </li>
  @else
      <li class="nav-item">
      <a style="color:black;" class="nav-link js-scroll-trigger" href="{{url('login')}}">{{trans('app.login')}}</a>
    </li>    
  @endif
</ul>
<div class="dropdown" style="display:none;">
  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">{{App::getLocale()}}
  <span class="caret"></span></button>
  <ul class="dropdown-menu" style="text-align:left;width:50%; float:left;margin-left:10%;">
    @if(App::getLocale()=='fr')
    <li><a  style="color:black;" href="{{url('language/en')}}">En</a></li>
    @else
    <li><a style="color:black;" href="{{url('language/fr')}}">Fr</a></li>
    @endif
    
    
  </ul>
</div>

        </div>
      </div>
    </nav>