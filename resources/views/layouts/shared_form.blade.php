 {{csrf_field()}}
          <div class="form-group">
            <input placeholder="title" type="text" name="title" class="form-control" value="">
          </div>
           <div class="form-group">
            <input placeholder="subtitle" type="text" name="subtitle" class="form-control" value="">
          </div>
          <div class="form-group">
            <select class="form-control" name="cat_id" value="">
              <option value="1">{{trans('app.webdesign')}}
              </option>
             <option value="2">{{trans('app.uidesign')}}
             </option>
             <option value="3">{{trans('app.logodesign')}}
             </option>
             <option value="4">{{trans('app.posterdesign')}}
             </option>
            </select>
          </div>
          <div class="form-group">
            <label for="editor">Blog content</label>
            <textarea name="content"  class="form-control" id="editor">
             
            </textarea>
          </div>
          <div class="form-group">
            <input id="input-id" data-show-upload="false" type="file"  class="file" name="image">
            
          </div>
          <div class="form-group">
            <input placeholder="tags, separate by [,]" type="text" name="tags" class="form-control" value="">
          </div>