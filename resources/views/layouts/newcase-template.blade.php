<!DOCTYPE html>
<html lang="en">

  @include('layouts.newcase_head')
  
  <body id="page-top">

   @include('layouts.nav')


    </header>-->

    <!-- Services -->
    @yield('pricing')

    

    <section id="services">

      <div class="container">
        
        <div class="row">
    
      @yield('content')
    </div>
  </div>
    </section>

<!-- my latest posts-->

  

   @include('layouts.footer')

   
   @include('layouts.new_case_js')
   <script src="{{asset('js/literallycanvas.js')}}"></script>
    <script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";

  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "{{trans('app.submit')}}";
  } else {
    document.getElementById("nextBtn").innerHTML = "{{trans('app.next')}}";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
    
}

function nextPrev(n) {
  // This function will figure out which tab to display
  
  var x = document.getElementsByClassName("tab");
  var div=document.getElementsByTagName('div');
  //console.log(n);
  if(n==1)
  {
    localStorage.setItem('logo_image',lc.getImage().toDataURL());
    document.getElementById('hidden_image').value=localStorage.getItem('logo_image');
    document.getElementById('logo_image').src=localStorage.getItem('logo_image');
    var company_name=document.getElementById('companyname').value;
    var companyIndustry=document.getElementById('companyIndustry').value;
    if(company_name!="" && companyIndustry!=""){
      localStorage.setItem('company_name',company_name);
     localStorage.setItem('companyIndustry',companyIndustry);
     document.getElementById('company').innerHTML="{{trans('app.company_name')}}: " +localStorage.getItem('company_name');
     document.getElementById('industry').innerHTML="{{trans('app.company_industry')}}: "+localStorage.getItem('companyIndustry')
    }
    
    var client_name=document.getElementById('clientName').value;
    var client_email=document.getElementById('clientEmail').value;
    if(client_name!="" && client_email!="")
    {

      localStorage.setItem('client_email',client_email);
      localStorage.setItem('client_name',client_name);
      document.getElementById('client_name').innerHTML="{{trans('app.your_name')}}: "+localStorage.getItem('client_name');
      document.getElementById('client_email').innerHTML="{{trans('app.your_email')}}: "+localStorage.getItem('client_email');


    }
    
    var booking_Date=document.getElementById('booking').value;
    var extra_days=document.getElementById('extra_days').value;
    
    if (booking_Date!="" && extra_days!="") {

      localStorage.setItem('booking',booking_Date);
    localStorage.setItem('extra_days',extra_days);
    document.getElementById('booking_date').innerHTML="{{trans('app.booking_date')}}: "+localStorage.getItem('booking');
    document.getElementById('booking_date_extra').innerHTML="{{trans('app.you_can_wait')}}: "+localStorage.getItem('extra_days');
    }
    
    var text=document.getElementById('more').value;
    if (text!="") {
      localStorage.setItem('more',text);
      document.getElementById('text').innerHTML="{{trans('app.your_tought')}}: "+localStorage.getItem('more');
    }
    
    
    //console.log(localStorage.getItem('logo_image'));
  } 
  if (n==2) {
    
  }

  if (n==3) {

    
  }
  if (n==4) {
    
  }
  if(n==5){
    


  }
  if (n==6) {
    

  }
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>

<script>
        var lc=LC.init(
            document.getElementsByClassName('my-drawing')[0],
            {imageURLPrefix: 'img'
            }

        );
      
      var subscribe=lc.on('drawingChange',function(){
        localStorage.setItem('drawing', JSON.stringify(lc.getSnapshot()));
        var image=lc.getImage();

        console.log(image);
      });
      window.onload=function(){
        if(localStorage.getItem('drawing')!="");
         // console.log(localStorage.getItem('drawing'));
        
      }
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
  var current = 1,current_step,next_step,steps;
  steps = $("fieldset").length;
  $(".next").click(function(){
    current_step = $(this).parent();
    next_step = $(this).parent().next();
    next_step.show();
    current_step.hide();
    setProgressBar(++current);
  });
  $(".previous").click(function(){
    current_step = $(this).parent();
    next_step = $(this).parent().prev();
    next_step.show();
    current_step.hide();
    setProgressBar(--current);
  });
  setProgressBar(current);
  // Change progress bar action
  function setProgressBar(curStep){
    var percent = parseFloat(100 / steps) * curStep;
    percent = percent.toFixed();
    $(".progress-bar")
      .css("width",percent+"%")
      .html(percent+"%");   
  }
});

    $( function() {
    var availableTags = [
"Accountants",
"Advertising/Public Relations",
"Aerospace, Defense Contractors",
"Agribusiness",
"Agricultural Services & Products",
"Agriculture",
"Air Transport",
"Air Transport Unions",
"Airlines",
"Alcoholic Beverages",
"Alternative Energy Production & Services",
"Architectural Services",
"Attorneys/Law Firms",
"Auto Dealers",
"Auto Dealers, Japanese",
"Auto Manufacturers",
"Automotive",
"Banking, Mortgage",
"Banks, Commercial",
"Banks, Savings & Loans",
"Bars & Restaurants",
"Beer, Wine & Liquor",
"Books, Magazines & Newspapers",
"Broadcasters, Radio/TV",
"Builders/General Contractors",
"Builders/Residential",
"Building Materials & Equipment",
"Building Trade Unions",
"Business Associations",
"Business Services",
"Cable & Satellite TV Production & Distribution",
"Candidate Committees ",
"Candidate Committees, Democratic",
"Candidate Committees, Republican",
"Car Dealers",
"Car Dealers, Imports",
"Car Manufacturers",
"Casinos / Gambling",
"Cattle Ranchers/Livestock",
"Chemical & Related Manufacturing",
"Chiropractors",
"Civil Servants/Public Officials",
"Clergy & Religious Organizations", 
"Clothing Manufacturing",
"Coal Mining",
"Colleges, Universities & Schools",
"Commercial Banks",
"Commercial TV & Radio Stations",
"Communications/Electronics",
"Computer Software",
"Conservative/Republican",
"Construction",
"Construction Services",
"Construction Unions",
"Credit Unions",
"Crop Production & Basic Processing",
"Cruise Lines",
"Cruise Ships & Lines",
"Dairy",
"Defense",
"Defense Aerospace",
"Defense Electronics",
"Defense/Foreign Policy Advocates",
"Democratic Candidate Committees",
"Democratic Leadership PACs",
"Democratic/Liberal",
"Dentists",
"Doctors & Other Health Professionals",
"Drug Manufacturers",
"Education",
"Electric Utilities",
"E-learning",
"Electronics Manufacturing & Equipment",
"Electronics, Defense Contractors",
"Energy & Natural Resources",
"Entertainment Industry",
"Environment", 
"Farm Bureaus",
"Farming",
"Finance / Credit Companies",
"Finance, Insurance & Real Estate",
"Food & Beverage",
"Food Processing & Sales",
"Food Products Manufacturing",
"Food Stores",
"For-profit Education",
"For-profit Prisons",
"Foreign & Defense Policy", 
"Forestry & Forest Products",
"Foundations, Philanthropists & Non-Profits",
"Funeral Services",
"Gambling & Casinos",
"Gambling, Indian Casinos",
"Garbage Collection/Waste Management",
"Gas & Oil",
"Gay & Lesbian Rights & Issues",
"General Contractors",
"Government Employee Unions",
"Government Employees",
"Gun Control", 
"Gun Rights", 
"Health",
"Health Professionals",
"Health Services/HMOs",
"Hedge Funds",
"HMOs & Health Care Services",
"Home Builders",
"Hospitals & Nursing Homes",
"Hotels, Motels & Tourism",
"Human Rights",
"Ideological/Single-Issue",
"Indian Gaming",
"Industrial Unions",
"Insurance",
"Internet",
"Israel Policy",
"Labor",
"Lawyers & Lobbyists",
"Lawyers / Law Firms",
"Leadership PACs", 
"Liberal/Democratic",
"Liquor, Wine & Beer",
"Livestock",
"Lobbyists",
"Lodging / Tourism",
"Logging, Timber & Paper Mills",
"Manufacturing, Misc",
"Marine Transport",
"Meat processing & products",
"Medical Supplies",
"Mining",
"Misc Business",
"Misc Finance",
"Misc Manufacturing & Distributing", 
"Misc Unions", 
"Miscellaneous Defense",
"Miscellaneous Services",
"Mortgage Bankers & Brokers",
"Motion Picture Production & Distribution",
"Music Production",
"Natural Gas Pipelines",
"Newspaper, Magazine & Book Publishing",
"Non-profits, Foundations & Philanthropists",
"Nurses",
"Nursing Homes/Hospitals",
"Nutritional & Dietary Supplements",

"Pharmaceutical Manufacturing",
"Pharmaceuticals / Health Products",
"Phone Companies",
"Physicians & Other Health Professionals",
"Postal Unions",
"Poultry & Eggs",
"Power Utilities",
"Printing & Publishing",
"Private Equity & Investment Firms",
"Pro-Israel", 
"Professional Sports, Sports Arenas & Related Equipment & Services",
"Progressive/Democratic",
"Public Employees",
"Public Sector Unions", 
"Publishing & Printing",

"Radio/TV Stations",
"Railroads",
"Real Estate",
"Record Companies/Singers",
"Recorded Music & Music Production",
"Recreation / Live Entertainment",
"Religious Organizations/Clergy",
"Republican Candidate Committees", 
"Republican Leadership PACs",
"Republican/Conservative",
"Residential Construction",
"Restaurants & Drinking Establishments",
"Retail Sales",
"Retired", 
"Savings & Loans",
"Schools/Education",
"Sea Transport",
"Securities & Investment",
"Special Trade Contractors",
"Sports, Professional",
"Steel Production", 
"Stock Brokers/Investment Industry",
"Student Loan Companies",
"Sugar Cane & Sugar Beets",
"Teachers Unions",
"Teachers/Education",
"Telecom Services & Equipment",
"Telephone Utilities",
"Textiles",
"Timber, Logging & Paper Mills",
"Tobacco",
"Transportation",
"Transportation Unions", 
"Trash Collection/Waste Management",
"Trucking",
"TV / Movies / Music",
"TV Production",
"Unions, Airline",
"Unions, Building Trades",
"Unions, Industrial",
"Unions, Misc",
"Unions, Public Sector",
"Unions, Teacher",
"Unions, Transportation",
"Universities, Colleges & Schools",
"Vegetables & Fruits",
"Venture Capital",
"Waste Management",
"Wine, Beer & Liquor",
"Women's Issues ",
    ];
    $( "#company_industry" ).autocomplete({
      source: availableTags
    });
  } );

$('#editor').summernote({
  height: 300,                 // set editor height
  minHeight: null,             // set minimum height of editor
  maxHeight: null,             // set maximum height of editor
  focus: true                  // set focus to editable area after initializing summernote
});
  </script>
  </body>

</html>
