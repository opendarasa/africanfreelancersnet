<!DOCTYPE html>
<html >
<head>
  @include('layouts.head')

</head>
  

  <body id="page-top">

    @include('layouts.nav')

    <!-- Header -->
    <header class="masthead">
      <div class="container">
      
      
      <div class="intro-text" style="color:black;">
        <div class="box col-md-12" style="background-color: rgba(255, 255, 255, 0.5); width:100%;">
          <div class="intro-lead-in">{{trans('app.welcome_message')}}</div>
          <div class="intro-heading text-uppercase" style="font-size:30px;">{{trans('app.welcome_message_continue')}}</div>
        </div>
         @include('shared.search-form')
        </div>
      
      </div>
      
    </header>

   
<!-- my latest posts-->

    <!-- About -->
   
<div id="blog">
<section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.latest_bog_post')}}</h2>
            
          </div>
        </div>
        
         @yield('blog')
        
      </div>
      
    </section>
</div>
    
    @include('layouts.footer')

    <!-- Portfolio Modals -->

    @yield('modals')

    @include('layouts.index_js')

  </body>

</html>
