 <!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <!-- piexif.min.js is needed for auto orienting image files OR when restoring exif data in resized images and when you
    wish to resize images before upload. This must be loaded before fileinput.min.js -->
<script src="{{asset('js/piexif.min.js')}}" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
    This must be loaded before fileinput.min.js -->
<script src="{{asset('js/sortable.min.js')}}" type="text/javascript"></script>
<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for 
    HTML files. This must be loaded before fileinput.min.js -->
<script src="{{asset('js/purify.min.js')}}" type="text/javascript"></script>
<!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js 
   3.3.x versions without popper.min.js. -->
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/fileinput.min.js')}}"></script>
<!-- optionally uncomment line below for loading your theme assets for a theme like Font Awesome (`fa`) -->
<!-- script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/themes/fa/theme.min.js"></script -->
<!-- optionally if you need translation for your language then include  locale file as mentioned below -->
<script src="{{asset('js/fr.min.js')}}"></script>

<script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('js/jquery-ui.js')}}">
    
</script>
    <!-- Plugin JavaScript -->
<script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Contact form JavaScript -->
<script src="{{asset('js/jqBootstrapValidation.js')}}"></script>
<script src="{{asset('js/contact_me.js')}}"></script>

    <!-- Custom scripts for this template -->
<script src="{{asset('js/agency.min.js')}}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

<script type="text/javascript">
	
  
          $(document).ready(function() {

         $("#myfile").fileinput({
        showPreview:true,
        showUpload: false,
        elErrorContainer: '#kartik-file-errors',
        allowedFileExtensions: ["jpg", "png", "gif"],
        @if(isset($blog->image))
         <?php 
         $path=public_path('img/blog');
          $image=$path.'/'.$blog->image;

          

         
         ?>
         initialPreview: [
        "<img src='{{asset('/img/blog/'.$blog->image)}}' class='file-preview-image' title='Desert' width='120' >"
       ],
       initialPreviewAsData: false, // identify if you are sending preview data only and not the raw markup
       initialPreviewFileType: 'image',
        initialPreviewConfig: [
        {caption: "{{$blog->image}}", size:{{filesize($image)}}, width: "120px", url: "{{$image}}"},
         ],
        
        @elseif(isset($work->image))
         <?php 
         $path=public_path('img/portfolio');
          $image=$path.'/'.$work->image;
         
         ?>
         initialPreview: [
        "<img src='{{asset('/img/portfolio/'.$work->image)}}' class='file-preview-image' title='Desert' width='120' >"
       ],
       initialPreviewAsData: false, // identify if you are sending preview data only and not the raw markup
       initialPreviewFileType: 'image',
        initialPreviewConfig: [
        {caption: "{{$work->image}}", size:{{filesize($image)}}, width: "120px", url: "{{$image}}"},
         ],
        @endif
    });
});
          
  $("#sort").change(function(){
   if($(this).val()=='Oldest'){
    //alert()
    $("#desc").css({display:"none"});
    $("#asc").css({display:"none"});
   }
   else if($(this).val()=="Newest")
   {
   	//alert("alert");
    $("#asc").css({display:"none"});
    $("#desc").css({display:"block"});
   }
  });
</script>

