<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="vendor/jquery/jquery.min.js"></script>
<style>
body {font-family: Arial, Helvetica, sans-serif;
}

/* Full-width input fields */
input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

/* Center the image and position the close button */
.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
    position: relative;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 25px;
    top: 0;
    color: #000;
    font-size: 35px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}

/* Add Zoom Animation */
.animate {
    -webkit-animation: animatezoom 0.6s;
    animation: animatezoom 0.6s
}
#login_button{
  margin-top: 5%;
  margin-left: 20%;
  margin-right:20%;
  width:100%;
  padding-right:30%;
  padding-left:30%;
  text-align:center;
  border-radius:6px;
  background-color: #4B65C8;
  font-size: 15px;
  font-weight: bolder;
}

@-webkit-keyframes animatezoom {
    from {-webkit-transform: scale(0)} 
    to {-webkit-transform: scale(1)}
}
    
@keyframes animatezoom {
    from {transform: scale(0)} 
    to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}
</style>
</head>
<body>

<!--<h2>{{trans('app.login')}}</h2>-->
<!--<img src="https://i.gifer.com/Jv9a.gif" style=" width:80%;margin-bottom:3%;display: inline-block; ">-->
<button id="login_button" onclick="document.getElementById('id01').style.display='block'" style="width:auto; display:inline-block;">@yield('login_button')</button>

<div id="id01" class="modal">
  
  <form class="modal-content animate" method="post" action="{{route('login')}}">
    @csrf
    <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
      <img src="img/500design.png" alt="Avatar" class="avatar" style="width:20%;height: 20%;">
    </div>

    <div class="container">
      <label for="uname"><b>{{trans('app.email')}}</b></label>
      <input class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" type="text" placeholder="{{trans('app.email')}}"  required>

      @if ($errors->has('email'))
        <span class="invalid-feedback">
        <strong>{{ $errors->first('email') }}</strong>
        </span>
      @endif

      <label for="psw"><b>{{trans('app.password')}}</b></label>
      <input class="{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" placeholder="{{trans('app.enter_password')}}" name="password" required>
      @if ($errors->has('password'))
       <span class="invalid-feedback">
         <strong>{{ $errors->first('password') }}</strong>
       </span>
      @endif
        
      <button style="background-color:#4B65C8" type="submit">{{trans('app.login')}}</button>
      <label>
        <input type="checkbox" checked="checked" name="remember">{{trans('app.rebember_me')}}
      </label>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <a href="{{url()->previous()}}"  class="cancelbtn">{{trans('app.cancel')}}</a>
      <span class="psw"><a href="{{ route('password.request') }}">{{trans('app.password')}} {{trans('app.forgot')}}?</a></span>

    </div>
    <div class="container" style="text-align:center;">
       <a href="{{url('home#contact')}}" class="login_button" type="button">
          {{trans('app.join_the_community')}}
    </a> 
    </div>
    
  </form>
  
</div>

<script>
// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
window.onload=function(){
  $("#login_button").click();
}
</script>

</body>
</html>
