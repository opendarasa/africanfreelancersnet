<!-- Contact -->

    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.leave_me_message')}}</h2>
            <h3 class="section-subheading text-muted">{{trans('app.i_always_reply')}}</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            @if(session()->has('status'))
              <div class="alert alert-success">
                {{session('status')}}
              </div>
            @endif
            <form id="contactForm" name="sentMessage" novalidate="novalidate" action="{{url('message')}}" method="post">
              {{csrf_field( )}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input name="name" class="form-control form-control-lg{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" type="text" placeholder="{{trans('app.your_name')}} *" value="{{old('name')}}" required>
                    @if ($errors->has('name'))
                        <span class="help-block text-danger" style="color:red" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <input name="email" class="form-control form-control-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" type="email" placeholder="{{trans('app.your_email')}} *" value="{{old('email')}}" required>
                    @if ($errors->has('email'))
                        <span class="help-block text-danger" style="color:red" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <input name="phone" class="form-control form-control-lg{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{old('phone')}}" id="phone" type="tel" placeholder="{{trans('app.your_phone')}} eg: +226xxxxxxxx" >
                     @if ($errors->has('email'))
                        <span class="help-block text-danger" style="color:red" role="alert">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea name="message" class="form-control  form-control-lg{{ $errors->has('message') ? ' is-invalid' : '' }}"  id="message" placeholder="{{trans('app.your_message')}} *" required>{{old('message')}}</textarea>
                    @if ($errors->has('message'))
                        <span class="help-block text-danger" style="color:red" role="alert">
                            <strong>{{ $errors->first('message') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit" style="background-color:#4B65C8;">{{trans('app.send_message')}}</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; {{env('APP_NAME')}} {{date('Y')}}</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>