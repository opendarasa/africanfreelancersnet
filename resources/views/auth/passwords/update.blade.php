@extends('layouts.newcase-template')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(session()->has('errorMsg'))
                    <div class="alert alert-danger">
                        <span>{{session('errorMsg')}}</span>
                    </div>
                @endif
                @if(session()->has('status'))
                    <div class="alert alert-success">
                        <span>{{session('status')}}</span>
                    </div>
                @endif
                <div class="card-header">{{ __('app.reset_password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('update-password') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="currentPassword" class="col-md-4 col-form-label text-md-right">{{ __('app.current_password') }}</label>

                            <div class="col-md-6">
                                <input name="currentPassword" id="currentPassword" type="password" class="form-control{{ $errors->has('currentPassword') ? ' is-invalid' : '' }}" name="email" value="{{  old('currentPassword') }}" required autofocus>

                                @if ($errors->has('currentPassword'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('currentPassword') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('app.new_password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('app.confirm_password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" style="background-color:#4B65C8;">
                                    {{ __('app.reset_password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
