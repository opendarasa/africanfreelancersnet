@extends('layouts.newcase-template')


@section('content')

      <div class="container">
        
          <div class="col-sm-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.new_blog')}}</h2>
            
          </div>
        
        
      <div class="col-sm-12">
        @if($errors->any())
        <div class="alert alert-danger">
          {{$errors->first()}}
        </div>
        @endif
           <!--<form method="post" action="{{route('blog.store')}}" enctype="multipart/form-data">-->
       {{Form::open( array(
             'route' => 'blog.store', 
             'class' => 'form', 
             'files' => true))}}

           @include('shared.shared_form')

          <div class="form-group">
            {{Form::submit(trans('app.save'),['class'=>'btn btn-info pull-right','style'=>'background-color:#4B65C8;'])}}
            <!--<button type="submit" class="btn btn-info pull-right" name="save">
              Save
            </button>-->
            
          </div>
           {{Form::close()}}
         <!--</form>-->
      </div>
        
        
      </div>
    
  
@endsection