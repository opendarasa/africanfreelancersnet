@extends('layouts.newcase-template')

@section('content')
@include('layouts.blog_css')
<div class="row">
  <div class="leftcolumn">
    <div class="card">
      <?php $author=App\User::find($currentBlog->user_id); ?>
     
      <h2>{{$currentBlog->title}},
        </h2>
        
      <h5>{{$currentBlog->subtitle}}, {{$currentBlog->created_at}}</h5>
      <div class="fakeimg">
      	<img src="{{asset('img/blog/'.$currentBlog->image)}}" class="blogmainimg">
      </div>
      <p>{!!$currentBlog->content!!}</p>
      <p>
        <div class="row">
          
          <ul class="social-icons">
            <li class="share">
              
            </li>
            <li class="facebook">
              <i class="fa fa-eye"></i>
              <span>
                {{$currentBlog->views}}
              </span>
            </li>
            <li class="google-plus">
              <a onclick="voteArticle('{{$currentBlog->id}}',1)" title="up-vote">
                <i class="fa fa-thumbs-up" style="color:#4B65C8"></i>
              </a>   
            </li>
            <li class="linkedin">
              <a onclick="voteArticle('{{$currentBlog->id}}',-1)" title="down-vote">
                <i class="fa fa-thumbs-down" style="color:#4B65C8"></i>
              </a>   
            </li>
             <li class="twitter">
              <strong >{{trans('app.revelancy')}}:</strong>
              <i class="fa fa-vote">
                
              </i> 
              <span id="thevotes">
                {{$currentBlog->votes}}
              </span>
            </li>
          </ul>
        </div>
        
        <ul class="social-icons">
          <li class="share">
            <h4>
          {{trans('app.share')}}:
          </h4>
          </li>
            <li class="facebook">
                <a href="#" onclick="shareOnFacebook();"><i class="fa fa-facebook"></i></a>
            </li>
            <li class="google-plus">
                <a href="#" onclick="shareOnGooglePlus();"><i class="fa fa-google-plus"></i></a>
            </li>
            <li class="linkedin">
                <a href="#" onclick="shareOnLinkedIn();"><i class="fa fa-linkedin"></i></a>
            </li>
            <li class="twitter">
                <a href="#" onclick="shareOnTwitter();"><i class="fa fa-twitter"></i></a>
            </li>
          </ul>
          
        
      </p>
    </div>
    
  </div>
  <div class="rightcolumn">
    <div class="card">

      <h3>{{trans('app.author')}}</h3>
      
      <div class="fakeimg">
        <a href="{{url('users/'.$user->username)}}">
          <img src="{{asset('img/users/'.$user->image)}}">
          <h5>{{$user->name}}</h5>
        </a>
        <p>
            {{trans('app.joined')}}
           <i class="fa fa-clock-o">
                  
               </i>
              {{(new Carbon\Carbon($user->created_at))->diffForHumans()}}</p>
      </div><br>
      
      
    </div>
    <div class="card" style="display:none;">
      <h3>{{trans('app.latest_work')}}</h3>
      @foreach($latest_works as $work)
      <div class="fakeimg">
      	<img src="{{asset('img/portfolio/'.$work->image)}}" class="sidebarimg">
      	<h5>{{mb_substr(strip_tags($work->title),0,20)}}..</h5>
      	<p>{{mb_substr(strip_tags($work->subtitle),0,60)}}..</p>
        <p>
           <i class="fa fa-clock-o">
                  
               </i>
              {{(new Carbon\Carbon($work->created_at))->diffForHumans()}}</p>
      </div><br>
      @endforeach
      
    </div>
    <div class="card">
      <h3>{{trans('app.popular_post')}}</h3>
      @foreach($blogs as $blog)
      <div class="fakeimg">
        <a href="{{route('blog.show',$blog->slug)}}">
        	<img class="sidebarimg" src="{{asset('img/blog/'.$blog->image)}}">
        	<p>{{mb_substr(strip_tags($blog->title),0,20)}}...</p><br>
        	<p>{{mb_substr(strip_tags($blog->subtitle),0,60)}}...</p>
      </a>
        <p>
          <i class="fa fa-clock-o">        
          </i>
              {{(new Carbon\Carbon($blog->created_at))->diffForHumans()}}</p>
      </div><br>
      @endforeach
      
    </div>
    <div class="card">
      <h3>{{trans('app.follow_us')}}</h3>
      <p>
      	<a href="">
      	<i class="fa fa-facebook">
      	 FACEBOOK
      </i>
        </a>
   </p>

      <p>
      	<a href="">
      	<i class="fa fa-twitter">
      		TWITTER
      	</i>
      </a>
      </p>
    </div>
  </div>
</div>

<script>
function shareOnFacebook() {
    window.open('http://www.facebook.com/share.php?u=' + encodeURIComponent(location.href), 'Facebook Share', 'width=640, height=480, scrollbars=yes');
}

function shareOnGooglePlus() {
    window.open('https://plus.google.com/share?url=' + encodeURIComponent(location.href), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
}

function shareOnLinkedIn() {
    window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(location.href), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
}

function shareOnTwitter() {
    window.open('http://twitter.com/home/?status=' + encodeURIComponent(document.title) + ' ' + encodeURIComponent(location.href), 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
}

function voteArticle(id,vote) {
  let currentVote = $("#thevotes").text().trim();
  console.log(parseInt(currentVote))
  $("#thevotes").html(parseInt(currentVote) + parseInt(vote))
 let data = {id:id,vote:vote}
 $.ajax({
  url:"{{url('vote')}}",
  method:'GET',
  data:data,
  success:function(response){
    console.log(response.data)
  },
  error:function(status){
    console.log(status)
  }

 });
}
</script>
@endsection