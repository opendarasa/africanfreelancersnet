    @extends('layouts.newcase-template')

    @section('content')

    <div class="container">
        <div class="row my-2">
            <div class="col-lg-4 order-lg-1 text-center">
                <img  src="{{asset('img/users/'.$user->image)}}" class="profileimage mx-auto img-fluid img-circle d-block" alt="avatar">
                @if(Auth::check()&& Auth::user()->id==$user->id)
                <h6 class="mt-2">{{trans('app.upload_new_photo')}}</h6>
                <form method="post" action="{{url('change_pic')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <label for="file">
                    <input accept="image/png,jpg,gif,jpeg" data-show-upload="true" type="file" id="file" class="file" name="image">
                    </label>
                </form>
                @endif
            </div>
            <div class="col-lg-8 order-lg-2">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">{{trans('app.profile')}}</a>
                        @if(Session::has('username'))
                            {{session()->get('unsername')}}
                        @endif
                    </li>
                    @if(Auth::check())
                        @if(Auth::user()->id==$user->id || Auth::user()->admin == 1)
                            <li class="nav-item">
                                <a href="" data-target="#edit" data-toggle="tab" class="nav-link">{{trans('app.edit')}}</a>
                            </li>
                        @endif
                    @endif
                </ul>
                <div class="tab-content py-4">
                    <div class="tab-pane active" id="profile">
                        <h5 class="mb-3">{{$user->name}}</h5>
                        <span class="text-muted" style="margin-bottom:4%;"> {{$user->company}}</span>
                        <div class="row">
                            <div class="col-md-6">
                                <h6>{{trans('app.about')}}</h6>
                                <p>
                                    {{$user->description}}
                                </p>
                                <p>
                                    <i class="fa fa-map-marker"></i>
                                    {{$user->location}}
                                </p>
                                <p>
                                    <i class="fa fa-clock-o"></i>
                                    {{trans('app.joined').'  '.(new Carbon\Carbon($user->created_at))->diffForHumans()}}
                                </p>
                            </div>
                            <div class="col-md-6">
                                <?php $skills=explode(",",$user->skills); ?>

                                <h6>
                                    {{trans('app.recent_badges')}}
                                </h6>

                                @foreach($skills as $skill)
                                    @if($skill != '')
                                        <span class="badge badge-dark badge-pill">
                                            {{$skill}}
                                            @if(Auth::check() && Auth::user()->id == $user->id)
                                            <a onclick="return confirm('{{trans('app.are_you_sure_you_want_to_delete_skill')}}')" href="{{url('remove-skill/'.$skill)}}">
                                                <i class="fa fa-trash">
                                                    
                                                </i>
                                            </a>
                                            @endif
                                        </span>
                                    @endif
                                @endforeach
                                @if(Auth::check()&&Auth::user()->admin==1)
                                 <form class="form-inline" method="post" action="{{url('add_skills')}}">
                                    {{csrf_field()}}
                                     <div class="form-group">
                                        <input placeholder="{{trans('app.add_skills')}} {{trans('app.separate_with_commas')}}" type="text" name="skills" class="form-control" required>
                                        <input type="hidden" name="user_id" value="{{$user->id}}">

                                     </div>
                                     <div class="form-group">
                                         <button type="submit" class="btn btn-warning" style="background-color:#4B65C8; color:#FFF;">
                                             {{trans('app.save')}}
                                         </button>
                                     </div>
                                 </form>
                                @endif
                                <hr>
                                <span class="badge badge-success"><i class="fa fa-cog"></i> {{$user->experience}} {{trans('app.years_experience')}}</span>
                                <span class="badge badge-danger"><i class="fa fa-eye"></i>
                                <?php 
                                $user_view=App\User::find($user->id);
                                $user_view->views+=1;
                                $user_view->save();
                                ?>
                                {{$user_view->views}}</span>
                            </div>

                        </div>
                        <!--/row-->
                    </div>

                    <div class="tab-pane" id="edit">
                        @if($errors->any())  
                            <div class="alert alert-danger">
                            {!!$errors->first()!!}
                            </div>
                        @endif
                        {!! Form::model($user, [
                        'method' => 'PATCH',
                        'route' => ['users.update', $user->username]
                        ]) !!}

                        {{csrf_field()}}

                        {{Form::hidden('password',$user->password)}}
                        <div class="form-group row">
                            {{Form::label('name',trans('app.full_name'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
                            <div class="col-lg-9">
                                {{Form::text('name',null,['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('username',trans('app.username'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
                            <div class="col-lg-9">
                                {{Form::text('username',$user->username,['class'=>'form-control','disabled'=>'false'])}}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('email',trans('app.email'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
                            <div class="col-lg-9">
                                {{Form::email('email',null,['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('description',trans('app.description_user'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
                            <div class="col-lg-9">
                                {{Form::textarea('description',null,['class'=>'form-control','placeholder'=>trans('app.short_description'),'id'=>'bio','maxlength'=>'500','minlength'=>'100'])}}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('company',trans('app.company'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
                            <div class="col-lg-9">
                                {{Form::text('company',null,['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="form-group row">
                                {{Form::label('website',trans('app.website'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
                            <div class="col-lg-9">
                                {{Form::url('website',config('app.url').'/users/'.$user->username,['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="form-group row">
                                {{Form::label('location',trans('app.location'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
                            <div class="col-lg-9">
                                {{Form::text('location',null,['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('experience',trans('app.years_experience'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
                            <div class="col-lg-9">
                                {{Form::number('experience',null,['class'=>'form-control'])}}
                            </div>
                        </div>
                        @if(isset($user->skills)&& $user->skills!="")
                            <div class="form-group row">
                                {{Form::label('skills',trans('app.skills'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
                                <div class="col-lg-9">
                                    @foreach($skills as $skill)
                                        @if($skill != '')
                                            <span class="badge badge-dark badge-pill">
                                                {{$skill}}
                                                 @if(Auth::check() && Auth::user()->id == $user->id)
                                                    <a onclick="return confirm('{{trans('app.are_you_sure_you_want_to_delete_skill')}}')" href="{{url('remove-skill/'.$skill)}}">
                                                        <i class="fa fa-trash">
                                                            
                                                        </i>
                                                    </a>
                                                @endif
                                            </span>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @else
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label""></label>
                                <div class="col-lg-9">
                                    <p>
                                        {{trans('app.you_have_no_earned_skills_yet')}}
                                    </p>

                                </div>

                            </div>
                        @endif
                        @if(Auth::check() && Auth::user()->admin == 1)
                         <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label" for="admin">{{trans('app.admin')}}</label>
                                <div class="col-lg-9">
                                    <input id="admin" type="checkbox" name="admin" value="{{$user->admin}}" class="form-control" {{$user->admin?'checked':''}}>

                                </div>

                        </div>
                        @endif
                        <div class="form-group row">
                            <div class="col-lg-3">
                                
                            </div>
                            <div class="col-lg-4" style="margin-bottom:2%;">
                                <input type="reset" class="btn btn-secondary" value="{{trans('app.cancel')}}">
                                
                            </div>
                            <div class="col-lg-4" style="margin-left: 0%;">
                                <input type="submit" class="btn btn-primary" value="{{trans('app.save_changes')}}" style="background-color:#4B65C8">
                            </div>
                        </div>
                        {{Form::close()}}



                    </div>
                </div>
            
            </div>

        </div>
    </div>

    <section class="bg-light" id="portfolio" style="margin-top:0%;">
      <div class="container">
        <div class="row">
          @if(isset($blogs) && count($blogs)>0)
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.my_latest_posts')}}</h2>
          </div>
          @endif
        </div> 
         @if(isset($blogs))   
          @include('shared.user_blogs')
        @endif
      </div>
      
    </section>
    
    <script type="text/javascript">
    $("#hire_mobile").click(function(){
    console.log('enter');
    $.ajax({
    url:"/savecookie",
    type:"GET",
    data:{username:{{$user->username}}},
    success:function(data,status){
    console.log(status);
    },
    error:function(data,status){
    console.log(status);
    }

    })
    })
    </script>
    @endsection