@component('mail::message')
#{{trans('app.new_seo_case')}}

{{trans('app.see_msg')}}

@component('mail::button', ['url' =>config('app.url')])
{{trans('app.order_more')}}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
