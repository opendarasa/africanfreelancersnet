@component('mail::message')
# {{trans('app.new_account_intro')}}

{{trans('app.welcome_msg_body')}}
<br>
{{trans('app.email')}}   : {{$email}} ,<br>
{{trans('app.password')}} : {{$pass}}


@component('mail::button', ['url' =>config('app.url').'/login'])
{{trans('app.login')}}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
