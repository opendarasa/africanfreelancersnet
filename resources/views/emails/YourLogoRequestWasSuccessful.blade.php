@component('mail::message')
# {{trans('app.we_received_your_logo_request')}}

{{trans('app.new_logo_msg_body')}}

@component('mail::button', ['url' => config('app.url')])
{{trans('app.order_more')}}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
