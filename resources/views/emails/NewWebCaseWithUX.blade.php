@component('mail::message')
# New case with UX provided

You have a new Web Design case with UX already provided.

@component('mail::button', ['url' =>$ux])
Download UX
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
