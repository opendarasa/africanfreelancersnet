@component('mail::message')
# {{trans('app.new_seo_case_msg_title')}}

{{trans('app.new_seo_case_msg_body')}}

@component('mail::button', ['url' =>$fileUrl])
Download File
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
