@component('mail::message')
# {{trans('app.you_have_just_made_a_web_request')}}

{{trans('app.new_web_design_msg')}}

@component('mail::button', ['url' =>config('app.url')])
{{trans('app.visit_website')}}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
