@component('mail::message')
# New Web design request 

You have a new web design case. See details in attached file below.

@component('mail::button', ['url' => config('app.url')])
visit website
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
