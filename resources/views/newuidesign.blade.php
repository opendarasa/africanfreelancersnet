@extends('layouts.newcase-template')


@section('content')
<style type="text/css">
  #regiration_form fieldset:not(:first-of-type) {
    display: none;
  }
  </style>
<div class="col-sm-12" style="text-align:center;">
  <h3>{{trans('app.mobile_dev')}}</h3>
</div>
@if($errors->any())
	<div style="text-align:center;" class=" col-sm-12 alert alert-danger">
		{{$errors->first()}}
	</div>
	@endif
	@if(Session::has('ok'))
	<div style="text-align:center;" class=" col-sm-12 alert alert-success">
		{{trans('app.verify_email')}}
	</div>
	@endif
<div class="col-sm-12 progress">
	
    <div class="progress-bar progress-bar-striped bg-info active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  
  <div class="col-sm-12">
  <form id="regiration_form" novalidate action="{{url('newuicase')}}"  method="post" enctype="multipart/form-data">
  	{{csrf_field()}}
  <fieldset>
    <h2>Step 1: {{trans('app.tell_me_about_your_company')}}</h2>
    <div class="form-group">
    <label for="company_name">{{trans('app.company_name')}}*</label>
    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="{{trans('app.company_name')}}">
    </div>
    <div class="form-group">
    <label for="company_industry">
    {{trans('app.company_industry')}}*
  </label>
    <input type="text" class="form-control" id="company_industry" placeholder="{{trans('app.company_industry')}}" name="company_industry">
    </div>
    <input type="button" name="next_1" class="next btn btn-info" value="{{trans('app.next')}}" />
  </fieldset>
  <fieldset>
    <h2> Step 2:{{trans('app.your_personal_information')}}</h2>
    <div class="form-group">
    <label for="fName">{{trans('app.first_name')}}*</label>
    <input type="text" class="form-control" name="fName" id="fName" placeholder="{{trans('app.first_name')}}">
    </div>
    <div class="form-group">
    <label for="lName">{{trans('app.last_name')}}*</label>
    <input type="text" class="form-control" name="lName" id="lName" placeholder="{{trans('app.last_name')}}">
    </div>
    <input type="button" name="previous" class="previous btn btn-default" value="{{trans('app.previous')}}" />
    <input type="button" name="next" class="next btn btn-info" value="{{trans('app.next')}}" />
  </fieldset>
  <fieldset>
    <h2>Step 3:{{trans('app.contact_information')}}</h2>
    <div class="form-group">
    <label for="mob">{{trans('app.mobile')}}</label>
    <input type="text" class="form-control" id="mob" placeholder="{{trans('app.mobile')}}" name="mobile">
    </div>
    <div class="form-group">
    <label for="address">{{trans('app.email')}}*</label>
    <input type="email" name="email" class="form-control" id="address">
    </div>
    <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
    <input type="button" name="next" class="next btn btn-info" value="next" />
  </fieldset>
  <fieldset>
    <h2>Step 4:{{trans('app.about_your_project')}}</h2>
    <div class="form-group">
      <select class="form-control" name="type">
      <option value="Android">{{trans('app.android')}}</option>
      <option value="IOS">{{trans('app.ios')}}</option>
      </select>
    </div>
    <div class="form-group">
    <label for="text">{{trans('app.describe_your_project')}}</label>
    <textarea class="form-control" style="height:200px;" id="text" name="text"></textarea>
    </div>
    <div class="form-group">
    <label for="booking">{{trans('app.do_your_booking')}}*</label>
    <input type="date" name="booking" class="form-control" id="booking" min="{{Carbon\Carbon::now()->toDateString()}}">
    </div>
    <div class="form-group">
    <label for="file">{{trans('app.do_you_have_ux')}}*</label>
    <input type="file" name="file" class="file" id="file" accept=".zip" data-show-upload="false">
    </div>
    <input type="button" name="previous" class="previous btn btn-default" value="{{trans('app.previous')}}" />
    <input type="submit" name="submit" class="submit btn btn-success" value="{{trans('app.submit')}}" />
  </fieldset>
  </form>
</div>
  
@endsection