<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Le mot de passe doit comporter aux moins 6 lettres.',
    'reset' => 'Votre mot de passe a été changé!',
    'sent' => 'Nous vous avons envyé un lien par e-mail!',
    'token' => 'Ce lien de renouvellement de mot de passe est valide.',
    'user' => "cet compte n'existe pas dans nos données.",

];
