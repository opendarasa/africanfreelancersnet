<?php

return [
     //footer
	'leave_me_message'=>'Envoyez nous votre message',
	'i_always_reply'=>'Nous sommes toujours à l\'écoute!',
	'your_name'=>'Votre Nom',
    'name_validation_msg'=>'Entrez votre nom',
    'your_email'=>'Entrez votre email',
     'email_validation_msg'=>'Entrez un email valide',
    'your_message'=>'Votre Message. Si vous désirez rejoindre la communauté Bookun, veuillez inclure dans votre message votre profession et pourquoi vous souhaitez nous rejoindre.',
    'message_validation_msg'=>'Votre Message ne peut être vide',
   'send_message'=>'Envoyez',

   // shared_form 
   'title'=>'Titre du livre',
   'subtitle'=>'Préface, ex: décrivez en une ou deux phrases le contenu du livre. Max 400 charactères ',
   'select_type'=>'Choisir la catégorie du livre',
   'webdesign'=>'Dévelopement Web',
   'uidesign'=>'Dévelopement Mobile',
   'seo'=>'SEO',
   'content'=>'Résumé du livre',
   'blog_image'=>'Image de prévision, eg: la couverture du livre',
   'tags_input_placeholder'=>'mots clés, séparer les mots par [,], eg:',

   //modal

   'date'=>'Date de Publication',
   'client'=>'Nom du Client',
   'Category'=>'Type de Projet',
   'close'=>'Fermer projet',

   //Pricing

   'mobile_dev'=>'Dévelopement Mobile',
   'delivery_in'=>'Temps ',
   'weeks'=>'Semaines',
   'submit_project'=>'Soumettre Projet',
   'web_design'=>'Dévelopement Web',

   //sort

   'newest_first'=>'Les plus nouveaux', 
   'oldest_first'=>'Les plus Anciens',

   //testimony

   'what_my_client_think'=>'Ce que nos clients pensent',

   //shared.users

   'worked_at'=>'Expérience Professionelle',
   'years_experience'=>'Années d\'expérience',
   'skills'=>'Compétences',
   'no_skills-earned_yet'=>'Pas encore de Compétences',
   'view_user'=>'Voir auteur',
   'joined'=>'A rejoint',
   //index
   'welcome_message'=>'Découvrez l\'essence des livres en quelques mots!',
   'welcome_message_continue'=>'Explorez les résumés maintenant pour une lecture enrichissante!',
   'what_are_you_looking_for'=>'Quel est votre type de projet?',
   'web_dev'=>'Dévelopement Web',
   'portofolio'=>'Portfolio',
   'team'=>'Écrire',
   'see_more'=>'Voir plus',
   'latest_bog_post'=>'Nos dernières publications',

   //login 

   'email'=>'Email',
   'password'=>'Mot de passe',
   'login'=>'S\'identifier',
   'cancel'=>'Annuller',
   'forgot'=>'oublié',
   'rebember_me'=>'Rester connecté',

   //nav

   'menu'=>'Menu',
   'home'=>'Acceuil',
   'my_services'=>'Services',
   'see_all'=>'Voir tout',
   'my_work'=>'Portfolio',
   'blog'=>'Lire',
   'new_blog'=>'Nouveau article',
   'new_work'=>'Nouveau projet',
   'profile'=>'Profil',
   'new_user'=>'Ajouter utilisateur',

   //newcase-template

   'submit'=>'Sousmettre',
   'next'=>'Suivant',
   'previous'=>'Précédent',
   //create new blog

   'new_blog'=>'Nouveau article',
   'save'=>'Sauvergarder',
   //new work
   'new_work'=>'Ajouter nouveau projet',

   //edit blog or work
   'publish'=>'Publier',
   'unpublish'=>'Rendre privé',
   'preview'=>'Previsualiser',
   //submit seo
   'tell_me_about_your_company'=>'À propos de votre entreprise',
   'company_name'=>'Nom d\'entreprise',
   'company_industry'=>'Que fait votre entreprise? ',
   'your_personal_information'=>'Information personelles',
   'first_name'=>'Nom',
   'last_name'=>'Prénom',
   'contact_information'=>'Vos contacts',
   'mobile'=>'Numéro tel',
   'about_your_event'=>'À Propos de votre projet SEO',
   'describe_your_event'=>'Décrivez votre projet',
   'do_your_booking'=>'Choisir délai de livraison',
   'upload_your_pdf_files'=>'Sousmettre un fichier .zip ou PDF du cahier de charge',

   //submit mobile project

   'verify_email'=>'Nous vous avons envoyé un email',
   'android'=>'Android',
   'ios'=>'IOS',
   'about_your_project'=>'À Propos de votre projet',
   'describe_your_project'=>'Décrivez en quelques les points importants du projet',
   'do_you_have_ux'=>'Soumettre le cahier de charge en format .zip ou PDF (UX/UI)',

   //view blog

   'latest_work'=>'Nos dernières réalisation',
   'popular_post'=>'Les articles les plus lus',
   'author'=>'Auteur',
   'follow_us'=>'Suivez nous',
   'share'=>'Partager',
   'pricing'=>'Tarification',
   //order details
   'web_dev_order_detail'=>'Détails d\'un projet web',
   'client_email'=>'Email du client',
   'client_mobile'=>'Tel du client',
   'companyname'=>'Nom d\'entreprise ',
   'companyIndustry'=>'Secteur d\'opération',
   'booking_date'=>'Délai d\'execution',
   'project_description'=>'Description du projet',
   'mobile_dev_order_detail'=>'Détails d\'un projet d\'Application mobile',
   'seo_dev_order_detail'=>'Détails d\'un projet SEO', 
   'freelancer_name'=>'Nom du Free-lancer',
   'new_seo_case'=>'Détails de votre commande : Projet SEO',
   'seo_msg'=>'Nous avons réçu votre projet d\'analyse SEO et vous enverrons les meilleurs dévis et offres de nos free-lancers',
  'order_more'=>'Faire une autre commande',

  'your_ui_case_was_received'=>'Les Détails de votre projet d\'Application mobile',
  'your_ui_message'=>'Nous venons de recevoir votre projet de réalisation d\'une Application mobile et vous enverrons les dévis des meilleurs free-lancers au plus tard demain',
  'you_have_just_made_a_web_request'=>'Les Détails de votre commande de developpement Web',
  'new_web_design_msg'=>'Nous venons de recevoir votre projet de réalisation d\'un site web et vous enverrons les dévis des meilleurs free-lancers au plus tard demain.',
  //view user
  'upload_new_photo'=>'Changer Photo de profile',
  'recent_badges'=>'Compétences',
  'git_repos'=>'Github Repositories',
  'repos_yet'=>' Aucun Github Repositories',
   'full_name'=>'Nom Complet',
   'username'=>'Nom d\'Utilisateur',
   'email'=>'Email',
  'description_user'=>'À Propos de vous',
  'company'=>'Dernier poste occuppé',
  'website'=>'Site web personel',
  'skills'=>'Compétences',
  'you_have_no_earned_skills_yet'=>'Les Compétences sont données uniquement par Opendarasa.',
  'repos'=>'Repositories',
  'edit'=>'Modifier profile',
  'hire_me'=>'Embauche  moi',
  'about'=>'À Propos de l\'auteur',
  'select_project_type'=>'Choisir le type du projet',
  'reminder_about_pricing'=>'Les Prix listés sont standards et négotiables',
  'short_description'=>'Décrivez votre expérience professionelle',
  
// add user
  'welcome_subject'=>'Félicitations! Vous êtes maintenant auteur Certifié!',
  'new_account_intro'=>'Vous venez juste d\'être retenu comme membre de la communauté des auteurs de résumés de livre.',
  'welcome_msg_body'=>'Bienvenue dans la communauté Bookun ! Nous sommes ravis que vous souhaitiez faire partie de notre groupe d\'auteurs passionnés qui rédigent des résumés de livres. C\'est un plaisir d\'accueillir des esprits créatifs comme le votre au sein de notre communauté dédiée à la découverte et à la discussion littéraire.

N\'hésitez pas à partager vos idées, à discuter des livres qui vous inspirent et à collaborer avec d\'autres membres. Ensemble, nous créons un espace dynamique où la richesse des perspectives et des résumés contribue à élargir nos horizons littéraires.

Si vous avez des questions ou si vous voulez en savoir plus sur la façon dont nous fonctionnons, n\'hésitez pas à nous contacter. Nous sommes impatients de lire tes résumés et de partager cette aventure littéraire avec vous au sein de la communauté Bookun. Bienvenue encore, et que l\'inspiration littéraire soit avec vous ! 📚✨.
Bienvenue encore ! Vous pouvez trouver vos identifiants de connexion ci-dessous :',

  'enter_username'=>'Entrez nom d\'Utilisateur',
  'add_skills'=>'Ajouter compétences',

  'description'=>'Trouvez les meilleurs livres traduits et résumés par nos  volontaires!',
  'my_latest_posts'=>'mes derniers articles',
  'location'=>'Ville',
  'post_categories'=>[
    '1' => 'Agribusiness',
    '2' => 'Blockchain',
    '3' => 'Cryptocurrency',
    '4' => 'Dévelopement personel',
    '5' => 'E-commerce',
    '6' => 'Économy',
    '7' => 'Élévage',
    '8' => 'Finance',
    '9' => 'Intélligence Articificielle',
    '10'=> 'Marketing',
    '11' => 'Santé',
    '12' => 'Autres'

  ],
  'blog_created_successfuly'=>'Article créé avec succès!',
  'blog_edited_successfuly' => 'Article modifié avec succès!',
  'are_you_sure_you_want_publish'=>'Ëtes-vous sûre de vouloir publier cet article?',
  'edit_blog'=>'Modifier article',
  'only_you_can_see_this'=>'Cet article n\'est pas encore publié!',
  'are_you_sure_you_want_unpublish'=>'Ëtes-vous sûre de vouloir rendre cet article privé?',
  'our_best_authors_of_the_month'=>'Meilleurs auteurs',
  'our_recommendation'=>'Nos recommandations',
  'posted'=>'posté',
  'no_search_result'=>'Aucun livre ou article trouvé!',
  'search_book'=>'Chercher le résumé d\'un livre',
  'search'=>'Chercher',
  'write'=>'Écrire',
  'read'=>'Lire',
  'save_changes'=>'Sauvergarder',
  'enter_password'=>'Mot de passe',
  'admin'=>'Administrateur',
  'view_users'=>'Gestionnaire des utilisateurs',
  'new_message'=>'Vous avez un nouveau message sur Bouqun',
  'we_have_received_your_message'=>'Message reçu ! Nous vous répondrons bientôt. 📚🌟',
  'join_the_community'=>'Rejoindre la communauté des auteurs',
  'password_changed'=>'Mot de passe changé!',
  'wrong_password'=>'Mot de passe érroné!',
  'user_not_found'=>'Utilisateur introuvable',
  'something_wrong_happened'=>'Problème technique , essayer plus tard!',
  'reset_password'=>'Changer mot de passe',
  'current_password'=>'Mot de passe courant',
  'new_password'=>'Nouveau mot de passe',
  'confirm_password'=>'Confirmer mot de passe',
  'you_need_to_update_your_password'=>'Vous devez changer votre mot de passe avant de continuer',
  'change_password'=>'Change mot de passe',
  'revelancy'=>'Niveau de relevance',
  'Logout'=>'Se disconnecter',
  'are_you_sure_you_want_to_delete_skill'=>'Ëtes-vous sûre de vouloir supprimer cette compétence?',
  'separate_with_commas'=>' Separer par des virgules. Ex: python, php',
  'your_phone'=>'Tel / Cel',
  'keywords'=>'livres, résumés de livre, livre gratuit, ',
  'email_is_taken'=>'Cet email est déjà pris',




];