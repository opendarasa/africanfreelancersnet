<?php
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('modify-migration', function(){
  $result = DB::table('migrations')->where('id',13)->delete();
  $result = DB::table('migrations')->get();
  print($result);
});
Route::get('services',function(){
  return view('services');
});
Route::get('language/{lang}','HomeController@changeLanguage');
Route::post('add_skills','UserController@addSkills');
Route::get('publishBlog/{slug}','HomeController@publishBlog');
Route::get('publishWork/{slug}','HomeController@publishWork');
Route::get('unpublishWork/{slug}','HomeController@unpublishWork');
Route::get('unpublishBlog/{slug}','HomeController@unpublishBlog');
Route::get('search','BlogController@searchByKeyword');
Route::get('vote','BlogController@voteArticle');
Route::post('update-password','UserController@changePassword');
Route::get('update-password',function(){
  return view('auth.passwords.update');
})->middleware('auth');

/*Route::get('portofolio',function(){
  return view('portofolio');
});
Route::get('blog',function(){
   return view('blog');
});*/
Route::get('new-web-case', function(){
  return view('newwebdesign');
});
Route::get('new-ui-case', function(){
  return view('newuidesign');
});

Route::post('newuicase','newcaseController@storeui');
Route::get('new-seo-case', function(){
  return view('newposterdesign');
});
Route::get('new-seo-case/{id}','newcaseController@storeSeoClient');
Route::get('new-web-case/{id}','newcaseController@storeWebClient');
Route::get('new-ui-case/{id}','newcaseController@storeMobileClient');
Route::post('newpostercase','newcaseController@storeposter');
Route::post('newwebcase','newcaseController@storeweb');
Route::get('/', function () {
    return redirect('home');
});
Route::get('newcase',function(){
	return view('newcase');
});
Route::get('remove-skill/{skill}','UserController@removeSkill');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('newcase','newcaseController@store');
Route::resource('blog','BlogController');
Route::post('message','HomeController@sendMessage');
Route::resource('portofolio','PortofolioController');
Route::get('add-user',function(){
  return redirect()->route('users.create');
})->middleware('auth');
Route::resource('users','UserController');
Route::get('/oauth/github', 'AuthController@redirectToProvider');
Route::get('/oauth/github/callback', 'AuthController@handleProviderCallback');
Route::post('change_pic','AuthController@changePic');
Route::get('/savecookie','newcaseController@savecookie');